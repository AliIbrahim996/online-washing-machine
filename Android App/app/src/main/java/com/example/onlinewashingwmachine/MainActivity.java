package com.example.onlinewashingwmachine;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    String email, full_name, ip;
    Bundle b;
    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.main_form:
                    FormFragment fragment = new FormFragment();
                    fragment.setArguments(b);
                    getSupportFragmentManager().beginTransaction().replace(R.id.main_frag, fragment)
                            .commit();
                    getSupportActionBar().setTitle("New order");
                    return true;
                case R.id.myOrders:
                    MyOrdersFragment ordersFragment = new MyOrdersFragment();
                    ordersFragment.setArguments(b);
                    getSupportFragmentManager().beginTransaction().replace(R.id.main_frag, ordersFragment)
                            .commit();
                    getSupportActionBar().setTitle("My orders");
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        Intent intent = getIntent();
        b = new Bundle();
        email = intent.getStringExtra("email");
        full_name = intent.getStringExtra("full_name");
        ip = intent.getStringExtra("ip");
        b.putString("email", email);
        b.putString("ip", ip);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.main_form);



    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drop_down, menu);
        return true;
    }

    //initialise toolbar actions
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            startActivity(new Intent(getApplicationContext(), LogInActivity.class));
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
