package com.example.onlinewashingwmachine.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.onlinewashingwmachine.Model.Order;
import com.example.onlinewashingwmachine.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AdminOrderRecycler extends RecyclerView.Adapter<AdminOrderRecycler.AdminViewHolder> {
    Context mContex;
    List<Order> orders;
    String status;
    String id;
    String ip;
    int pos;
    SharedPreferences sharedPreferences;

    class AdminViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView orderNum, orderStatus, orderTotalQuantity, washingStatus;
        CardView c;

        public AdminViewHolder(@NonNull View itemView) {

            super(itemView);
            itemView.setOnClickListener(this);
            orderNum = itemView.findViewById(R.id.orderNum);
            orderStatus = itemView.findViewById(R.id.orderStatus);
            orderTotalQuantity = itemView.findViewById(R.id.quantity);
            washingStatus = itemView.findViewById(R.id.washingStatus);
        }

        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContex);
            builder.setTitle("Change order status");
            Order order = orders.get(getAdapterPosition());
            id = order.getId();
            pos = getAdapterPosition();
            System.out.println("ID := "+id);
            builder.setMessage("Accept or Reject order!");
            builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    status = "accepted";
                    new updateTask().execute();

                }
            });
            builder.setNegativeButton("Reject", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    status = "rejected";
                    new updateTask().execute();
                }
            }).show();
        }
    }


    @NonNull
    @Override
    public AdminViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContex);
        View mView = inflater.inflate(R.layout.order_item, parent, false);
        return new AdminViewHolder(mView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull AdminViewHolder holder, int position) {
        Order order = orders.get(position);
        int c1 = mContex.getColor(R.color.Green);
        int c2 = mContex.getColor(R.color.RED);

        holder.orderNum.setText(order.getId());
        holder.washingStatus.setText(order.getWashingType());
        holder.orderTotalQuantity.setText(order.getTotalQuantity());
        holder.orderStatus.setText(order.getOrderStatus());
        switch (order.getOrderStatus()) {
            case "rejected":
                holder.orderStatus.setTextColor(c2);
                break;
            case "accepted":
                holder.orderStatus.setTextColor(c1);
                break;

        }
    }

    class updateTask extends AsyncTask<Void, Void, String> {

        StringBuilder stringBuilder;
        String responseMessage;
        int responseCode;

        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {
                URL url = new URL("http://" + sharedPreferences.getString("SERVER_IP", ip) +
                        "/wm/API/controller/updateStatus.php");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                System.out.println(url);
                urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(15000);
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                JSONObject uData = new JSONObject();
                uData.put("id", id);
                uData.put("status", status);
                System.out.println(uData);
                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(uData.toString());
                writer.flush();
                writer.close();
                os.close();
                urlConnection.connect();
                try {
                    responseCode = urlConnection.getResponseCode();
                    responseMessage = urlConnection.getResponseMessage();
                    if (responseCode != 200) {
                        System.out.println("Response Code: " + responseCode + "\nResponse Message :" + responseMessage);
                        return null;
                    }
                    System.out.println(urlConnection.getInputStream());
                    BufferedReader bufferedReader = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream()));
                    stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    System.out.println("Response Code: " + responseCode + "\nResponse Message :" + responseMessage);
                    return stringBuilder.toString();

                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {
            if (response == null) {
                Toast.makeText(mContex, "THERE WAS AN ERROR Response null", Toast.LENGTH_LONG).show();
                return;
            }
            if (responseCode != 200) {
                Toast.makeText(mContex, "THERE WAS AN ERROR response code is: " + responseCode, Toast.LENGTH_LONG).show();
                return;
            }
            String message = "";
            int flag = -2;

            try {
                JSONObject jsonObject = new JSONObject(response);
                System.out.println(jsonObject);
                flag = jsonObject.getInt("flag");
                message = jsonObject.getString("message");
                if (flag == 1) {
                    Toast.makeText(mContex, message, Toast.LENGTH_LONG).show();
                    orders.get(pos).setOrderStatus(status);
                    notifyDataSetChanged();
                }

            } catch (Exception e) {
                Toast.makeText(mContex, "There was an error" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public int getItemCount() {
        return orders.size();
    }

    public AdminOrderRecycler(Context mContex, List<Order> orders, SharedPreferences sharedPreferences, String ip) {
        this.mContex = mContex;
        this.orders = orders;
        this.sharedPreferences = sharedPreferences;
        this.ip = ip;
    }
}