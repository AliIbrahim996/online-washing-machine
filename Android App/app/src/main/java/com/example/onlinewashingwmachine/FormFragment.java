package com.example.onlinewashingwmachine;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class FormFragment extends Fragment {
    View mView;
    Context mContext;
    JSONObject data;
    SharedPreferences sharedPreferences;
    Bundle b;
    String ip, email, full_name, paymentMethod, cardId, loc, wType, status;
    CheckBox t_shirtCheck, trousers, shirt, wT,
            jumper, mP, wS;
    ImageView addT, minusT, addTro, minusTro, addS, minusS, addwT,
            jumperAdd, jumperMinus, mPAdd, mPMinus, wSAdd, wSMinus, minuswT;
    EditText tValue, troValue, sValue, wTValue, jumperValue, mPValue, wSValue,
            paypal_id, masterCard_id, visaCard_id, location;

    RadioButton fastW, normalW, paypalR, masterR, visaR;
    Button submit_button;
    TextInputLayout masterCard_idInput, paypal_idInput, visaCard_idInput;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.form_fragment, container, false);
        mContext = mView.getContext();
        sharedPreferences = mContext.getSharedPreferences("Preferences", mContext.MODE_PRIVATE);
        b = getArguments();
        ip = b.getString("ip");
        email = b.getString("email");
        // full_name = b.getString("full_name");

        visaCard_idInput = mView.findViewById(R.id.visaCard_idInput);
        paypal_idInput = mView.findViewById(R.id.paypal_idInput);
        masterCard_idInput = mView.findViewById(R.id.masterCard_idInput);

        submit_button = mView.findViewById(R.id.submit_button);
        submit_button.setOnClickListener(SendData());
        //Radio
        fastW = mView.findViewById(R.id.fastW);
        fastW.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                wType = "fast-washing";
            }
        });
        normalW = mView.findViewById(R.id.normalW);
        normalW.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                wType = "normal-washing";
            }
        });

        paypalR = mView.findViewById(R.id.payR);
        paypalR.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    masterR.setChecked(false);
                    visaR.setChecked(false);

                    paypal_id.setVisibility(View.VISIBLE);
                    masterCard_id.setVisibility(View.INVISIBLE);
                    visaCard_id.setVisibility(View.INVISIBLE);

                    masterCard_idInput.setVisibility(View.INVISIBLE);
                    paypal_idInput.setVisibility(View.VISIBLE);
                    visaCard_idInput.setVisibility(View.INVISIBLE);
                }

            }
        });
        masterR = mView.findViewById(R.id.masterR);
        masterR.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    paypalR.setChecked(false);
                    visaR.setChecked(false);

                    masterCard_idInput.setVisibility(View.VISIBLE);
                    paypal_idInput.setVisibility(View.INVISIBLE);
                    visaCard_idInput.setVisibility(View.INVISIBLE);

                    paypal_id.setVisibility(View.INVISIBLE);
                    masterCard_id.setVisibility(View.VISIBLE);
                    visaCard_id.setVisibility(View.INVISIBLE);
                }

            }
        });

        visaR = mView.findViewById(R.id.visaR);
        visaR.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    paypalR.setChecked(false);
                    masterR.setChecked(false);
                    paypal_id.setVisibility(View.INVISIBLE);
                    masterCard_id.setVisibility(View.INVISIBLE);
                    visaCard_id.setVisibility(View.VISIBLE);

                    masterCard_idInput.setVisibility(View.INVISIBLE);
                    paypal_idInput.setVisibility(View.INVISIBLE);
                    visaCard_idInput.setVisibility(View.VISIBLE);
                }
            }
        });

        //checkBox
        t_shirtCheck = mView.findViewById(R.id.t_shirtCheck);
        trousers = mView.findViewById(R.id.trousers);
        shirt = mView.findViewById(R.id.shirt);
        wT = mView.findViewById(R.id.wT);
        jumper = mView.findViewById(R.id.jumper);
        mP = mView.findViewById(R.id.mP);
        wS = mView.findViewById(R.id.wS);


        //EditText
        location = mView.findViewById(R.id.loc);
        tValue = mView.findViewById(R.id.tValue);
        tValue.setEnabled(false);
        tValue.setText("0");

        troValue = mView.findViewById(R.id.troValue);
        troValue.setEnabled(false);
        troValue.setText("0");

        sValue = mView.findViewById(R.id.sValue);
        sValue.setEnabled(false);
        sValue.setText("0");

        wTValue = mView.findViewById(R.id.wTValue);
        wTValue.setEnabled(false);
        wTValue.setText("0");

        jumperValue = mView.findViewById(R.id.jumperValue);
        jumperValue.setEnabled(false);
        jumperValue.setText("0");

        mPValue = mView.findViewById(R.id.mPValue);
        mPValue.setEnabled(false);
        mPValue.setText("0");

        wSValue = mView.findViewById(R.id.wSValue);
        wSValue.setEnabled(false);
        wSValue.setText("0");


        paypal_id = mView.findViewById(R.id.paypal_id);
        masterCard_id = mView.findViewById(R.id.masterCard_id);
        visaCard_id = mView.findViewById(R.id.visaCard_id);


        //AddBtn
        addT = mView.findViewById(R.id.addT);
        addT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldVal = tValue.getText().toString();
                int val = Integer.parseInt(oldVal) + 1;
                tValue.setText(val + "");
            }
        });
        addS = mView.findViewById(R.id.addS);
        addS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldVal = sValue.getText().toString();
                int val = Integer.parseInt(oldVal) + 1;
                sValue.setText(val + "");
            }
        });

        addTro = mView.findViewById(R.id.addTro);
        addTro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldVal = troValue.getText().toString();
                int val = Integer.parseInt(oldVal) + 1;
                troValue.setText(val + "");
            }
        });
        addwT = mView.findViewById(R.id.addwT);
        addwT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldVal = wTValue.getText().toString();
                int val = Integer.parseInt(oldVal) + 1;
                wTValue.setText(val + "");
            }
        });

        jumperAdd = mView.findViewById(R.id.jumperAdd);
        jumperAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldVal = jumperValue.getText().toString();
                int val = Integer.parseInt(oldVal) + 1;
                jumperValue.setText(val + "");
            }
        });

        mPAdd = mView.findViewById(R.id.mPAdd);
        mPAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldVal = mPValue.getText().toString();
                int val = Integer.parseInt(oldVal) + 1;
                mPValue.setText(val + "");
            }
        });

        wSAdd = mView.findViewById(R.id.wSAdd);
        wSAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldVal = wSValue.getText().toString();
                int val = Integer.parseInt(oldVal) + 1;
                wSValue.setText(val + "");
            }
        });

        //Minus Btn
        minusT = mView.findViewById(R.id.minusT);
        minusT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldVal = tValue.getText().toString();
                int val = Integer.parseInt(oldVal) - 1;
                if (val < 0)
                    tValue.setText(0);
                else
                    tValue.setText(val + "");
            }
        });

        minusTro = mView.findViewById(R.id.minusTro);
        minusTro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldVal = troValue.getText().toString();
                int val = Integer.parseInt(oldVal) - 1;
                if (val < 0)
                    troValue.setText(0);
                else
                    troValue.setText(val + "");

            }
        });

        minusS = mView.findViewById(R.id.minusS);
        minusS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldVal = sValue.getText().toString();
                int val = Integer.parseInt(oldVal) - 1;
                if (val < 0)
                    sValue.setText(0);
                else
                    sValue.setText(val + "");
            }
        });

        minuswT = mView.findViewById(R.id.minuswT);
        minuswT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldVal = wTValue.getText().toString();
                int val = Integer.parseInt(oldVal) - 1;
                if (val < 0)
                    wTValue.setText(0);
                else
                    wTValue.setText(val + "");

            }
        });

        jumperMinus = mView.findViewById(R.id.jumperMinus);
        jumperMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldVal = jumperValue.getText().toString();
                int val = Integer.parseInt(oldVal) - 1;
                if (val < 0)
                    jumperValue.setText(0);
                else
                    jumperValue.setText(val + "");
            }
        });

        mPMinus = mView.findViewById(R.id.mPMinus);
        mPMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldVal = mPValue.getText().toString();
                int val = Integer.parseInt(oldVal) - 1;
                if (val < 0)
                    mPValue.setText(0);
                else
                    mPValue.setText(val + "");
            }
        });

        wSMinus = mView.findViewById(R.id.wSMinus);
        wSMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldVal = wSValue.getText().toString();
                int val = Integer.parseInt(oldVal) - 1;
                if (val < 0)
                    wSValue.setText(0);
                else
                    wSValue.setText(val + "");
            }
        });

        return mView;
    }

    private void clearFileds() {
        //Edit text
        tValue.setText("0");
        wSValue.setText("0");
        mPValue.setText("0");
        jumperValue.setText("0");
        wTValue.setText("0");
        sValue.setText("0");
        troValue.setText("0");
        location.setText("");

        //checkBox
        t_shirtCheck.setChecked(false);
        trousers.setChecked(false);
        shirt.setChecked(false);
        wT.setChecked(false);
        jumper.setChecked(false);
        mP.setChecked(false);
        wS.setChecked(false);

        //Radio buttons
        paypalR.setChecked(false);
        masterR.setChecked(false);
        visaR.setChecked(false);

        paypal_id.setVisibility(View.INVISIBLE);
        masterCard_id.setVisibility(View.INVISIBLE);
        visaCard_id.setVisibility(View.INVISIBLE);
        masterCard_idInput.setVisibility(View.INVISIBLE);
        paypal_idInput.setVisibility(View.INVISIBLE);
        visaCard_idInput.setVisibility(View.INVISIBLE);

        fastW.setChecked(false);
        normalW.setChecked(false);


        //
    }

    private View.OnClickListener SendData() {
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    data = new JSONObject();
                    if (t_shirtCheck.isChecked()) {
                        data.put("t-shirt", tValue.getText().toString());
                    }
                    if (trousers.isChecked()) {
                        data.put("trousers", troValue.getText().toString());
                    }
                    if (shirt.isChecked()) {
                        data.put("shirt", sValue.getText().toString());
                    }
                    if (wT.isChecked()) {
                        data.put("woman_t-shirt", wTValue.getText().toString());
                    }
                    if (jumper.isChecked()) {
                        data.put("Jummper", jumperValue.getText().toString());
                    }
                    if (mP.isChecked()) {
                        data.put("man_pejama", mPValue.getText().toString());
                    }
                    if (wS.isChecked()) {
                        data.put("woman_shirt", wSValue.getText().toString());
                    }
                    if (paypalR.isChecked()) {
                        paymentMethod = "PayPal";
                        cardId = paypal_id.getText().toString();
                    }
                    if (masterR.isChecked()) {
                        paymentMethod = "Master-card";
                        cardId = masterCard_id.getText().toString();
                    }
                    if (visaR.isChecked()) {
                        paymentMethod = "VisaCard";
                        cardId = visaCard_id.getText().toString();
                    }
                    loc = location.getText().toString();
                    new AddOrderTask().execute();

                } catch (Exception e) {

                }
            }
        };
        return onClickListener;
    }

    class AddOrderTask extends AsyncTask<Void, Void, String> {

        StringBuilder stringBuilder;
        String responseMessage;
        int responseCode;

        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {
                URL url = new URL("http://" + sharedPreferences.getString("SERVER_IP", ip) +
                        "/wm/API/controller/insertOrder.php");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                System.out.println(url);
                urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(15000);
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                JSONObject uData = new JSONObject();
                uData.put("email", email);
                uData.put("cData", data);
                uData.put("paymentMethod", paymentMethod);
                uData.put("cardId", cardId);
                uData.put("status", "wating");
                uData.put("location", loc);
                uData.put("type", wType);
                System.out.println(uData);
                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(uData.toString());
                writer.flush();
                writer.close();
                os.close();
                urlConnection.connect();
                try {
                    responseCode = urlConnection.getResponseCode();
                    responseMessage = urlConnection.getResponseMessage();
                    if (responseCode != 201) {
                        System.out.println("Response Code: " + responseCode + "\nResponse Message :" + responseMessage);
                        return null;
                    }
                    System.out.println(urlConnection.getInputStream());
                    BufferedReader bufferedReader = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream()));
                    stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    System.out.println("Response Code: " + responseCode + "\nResponse Message :" + responseMessage);
                    return stringBuilder.toString();

                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {
            if (response == null) {
                Toast.makeText(mContext, "THERE WAS AN ERROR Response null", Toast.LENGTH_LONG).show();
                return;
            }
            if (responseCode != 201) {
                Toast.makeText(mContext, "THERE WAS AN ERROR response code is: " + responseCode, Toast.LENGTH_LONG).show();
                return;
            }
            String message = "";
            int flag = -2;

            try {
                JSONObject jsonObject = new JSONObject(response);
                flag = jsonObject.getInt("flag");
                message = jsonObject.getString("message");
                if (flag == 1) {
                    //Todo clear all fileds
                    clearFileds();
                }
            } catch (Exception e) {
                Toast.makeText(mContext, "There was an error" + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }


}
