package com.example.onlinewashingwmachine.Model;

import java.util.HashMap;

public class Order {
    HashMap<String, String> data;
    String washingType;
    String location;
    String paymentMethod;
    String cardID;
    String id;
    String orderStatus;

    public Order(String id, HashMap<String, String> data, String washingType, String location, String paymentMethod, String cardID) {
        this.data = data;
        this.washingType = washingType;
        this.location = location;
        this.paymentMethod = paymentMethod;
        this.cardID = cardID;
        this.id = id;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Order(String washingType, String id, String orderStatus) {
        this.washingType = washingType;
        this.id = id;
        this.orderStatus = orderStatus;
    }

    public Order(String washingType, String location, String paymentMethod, String cardID, String id) {
        this.washingType = washingType;
        this.location = location;
        this.paymentMethod = paymentMethod;
        this.cardID = cardID;
        this.id = id;
    }

    public String getTotalQuantity() {
        int total = 0;
        for (Object key : data.keySet()) {
            total += Integer.parseInt(data.get(key));
        }
        return total + "";
    }

    public HashMap<String, String> getData() {
        return data;
    }

    public void setData(HashMap<String, String> data) {
        this.data = data;
    }

    public String getWashingType() {
        return washingType;
    }

    public void setWashingType(String washingType) {
        this.washingType = washingType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
