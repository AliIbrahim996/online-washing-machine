package com.example.onlinewashingwmachine.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.example.onlinewashingwmachine.Model.Order;
import com.example.onlinewashingwmachine.R;

import java.util.List;

public class OrderRecylerAdapter extends RecyclerView.Adapter<OrderRecylerAdapter.OrderViewHolde> {
    Context mContex;
    List<Order> orders;
    String status;
    String id;

    public OrderRecylerAdapter(Context mContex, List<Order> orders) {
        this.mContex = mContex;
        this.orders = orders;
    }

    @NonNull
    @Override
    public OrderViewHolde onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContex);
        View mView = inflater.inflate(R.layout.order_item, parent, false);
        return new OrderViewHolde(mView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull OrderViewHolde holder, int position) {
        Order order = orders.get(position);
        id = order.getId();
        int c1 = mContex.getColor(R.color.Green);
        int c2 = mContex.getColor(R.color.RED);
        holder.orderNum.setText(id);
        holder.washingStatus.setText(order.getWashingType());
        holder.orderTotalQuantity.setText(order.getTotalQuantity());
        holder.orderStatus.setText(order.getOrderStatus());
        switch (order.getOrderStatus()) {
            case "rejected":
                holder.orderStatus.setTextColor(c2);
                break;
            case "accepted":
                holder.orderStatus.setTextColor(c1);
                break;

        }

    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    class OrderViewHolde extends RecyclerView.ViewHolder {
        TextView orderNum, orderStatus, orderTotalQuantity, washingStatus;

        public OrderViewHolde(@NonNull View itemView) {

            super(itemView);
            orderNum = itemView.findViewById(R.id.orderNum);
            orderStatus = itemView.findViewById(R.id.orderStatus);
            orderTotalQuantity = itemView.findViewById(R.id.quantity);
            washingStatus = itemView.findViewById(R.id.washingStatus);
        }
    }
}
