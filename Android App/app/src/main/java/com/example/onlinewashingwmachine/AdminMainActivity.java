package com.example.onlinewashingwmachine;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.onlinewashingwmachine.Adapter.AdminOrderRecycler;
import com.example.onlinewashingwmachine.Adapter.OrderRecylerAdapter;
import com.example.onlinewashingwmachine.Model.Order;;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class AdminMainActivity extends AppCompatActivity {

    RecyclerView orderRView;
    List<Order> orders;
    Context mContext;
    String ip;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_order_page);
        mContext = this;
        sharedPreferences = getSharedPreferences("Preferences", MODE_PRIVATE);
        Intent intent = getIntent();
        ip = intent.getStringExtra("ip");
        orderRView = findViewById(R.id.orderRView);
        orderRView.setHasFixedSize(true);
        orderRView.setLayoutManager(new LinearLayoutManager(this));
        new getAllOrders().execute();

    }

    class getAllOrders extends AsyncTask<Void, Void, String> {

        StringBuilder stringBuilder;
        String responseMessage;
        int responseCode;

        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {
                URL url = new URL("http://" + sharedPreferences.getString("SERVER_IP", ip) +
                        "/wm/API/controller/getAllIOrders.php");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                System.out.println(url);
                urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(15000);
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.connect();
                try {
                    responseCode = urlConnection.getResponseCode();
                    responseMessage = urlConnection.getResponseMessage();
                    if (responseCode != 200) {
                        System.out.println("Response Code: " + responseCode + "\nResponse Message :" + responseMessage);
                        return null;
                    }
                    System.out.println(urlConnection.getInputStream());
                    BufferedReader bufferedReader = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream()));
                    stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    System.out.println("Response Code: " + responseCode + "\nResponse Message :" + responseMessage);
                    return stringBuilder.toString();

                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {
            String message = "";
            JSONObject orders;
            int flag = -2;

            if (response == null) {
                Toast.makeText(mContext, "THERE WAS AN ERROR Response null", Toast.LENGTH_LONG).show();
                return;
            }
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (responseCode != 200) {
                    message = jsonObject.getString("message");
                    Toast.makeText(mContext, "THERE WAS AN ERROR response code is: " + responseCode
                            + "Message: " + message, Toast.LENGTH_LONG).show();
                    return;
                }
                flag = jsonObject.getInt("flag");

                if (flag == 1) {
                    orders = jsonObject.getJSONObject("orders");
                    JSONArray data = orders.getJSONArray("data");
                    collectData(data);
                }
            } catch (Exception e) {
                Toast.makeText(mContext, "There was an error" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void collectData(JSONArray data) throws JSONException {
        orders = new ArrayList<>();
        for (int i = 0; i < data.length(); i++) {
            JSONObject element = data.getJSONObject(i);
            String id = element.getString("id");
            String washingType = element.getString("washingType");
            String status = element.getString("status");
            String paymentMethod = element.getString("paymentMethod");
            String cardId = element.getString("cardId");
            JSONObject orderData = element.getJSONObject("data");
            HashMap<String, String> myData = new HashMap<String, String>();
            System.out.println(orderData);
            Iterator<String> orderIterator = orderData.keys();
            while (orderIterator.hasNext()) {
                String object = orderIterator.next();
                String qunatity = orderData.getString(object);
                myData.put(object, qunatity);
            }
            String location = element.getString("location");

            Order o = new Order(
                    id, myData, washingType, location, paymentMethod, cardId
            );
            o.setOrderStatus(status);
            orders.add(o);
        }
        AdminOrderRecycler adapter = new AdminOrderRecycler(this, orders, sharedPreferences, ip);
        orderRView.setAdapter(adapter);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drop_down, menu);
        return true;
    }

    //initialise toolbar actions
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            startActivity(new Intent(getApplicationContext(), LogInActivity.class));
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

