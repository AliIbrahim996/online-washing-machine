<?php

class User
{
    private PDO $conn;
    private string $table = 'user';
    //User Prop

    /**
     * `Id` int(11) NOT NULL,
     * `Full_name` varchar(255) DEFAULT NULL,
     * `email` varchar(255) DEFAULT NULL,
     * `password` varchar(255) DEFAULT NULL,
     * `userRole` int(11) NOT NULL
     */

    private string $id;
    private string $full_name;
    private string $userRole;
    private string $email;
    private string $password;

    /**
     * @return string
     */
    public function getDir()
    {
        return $this->dir;
    }


    public function __construct(PDO $db)
    {
        $this->conn = $db;
    }

    function registerUser($data)
    {
        // insert query
        $query = "INSERT INTO " . $this->table . "
            SET
                Full_name = ?,
                email = ?,
                password = ?,
                userRole = ?
               ";
        $stmt = $this->conn->prepare($query);
        // bind the values
        $password = password_hash($data->password, PASSWORD_BCRYPT);
        $stmt->bindParam(1, $data->full_name);
        $stmt->bindParam(2, $data->email);
        $stmt->bindParam(3, $password);
        $stmt->bindParam(4, $data->userRole);
        // execute the query, also check if query was successful
        try {
            $stmt->execute();
            //201 created
            http_response_code(201);
            return json_encode(array(
                "message" => "User registered successful",
                "flag" => 1));
        } catch (Exception $e) {
            http_response_code(400);
            return json_encode(array(
                "message" => "error: " . $e->getMessage()
            ));
        }

    }


    public function setEmail($email)
    {
        $this->email = $email;
        $this->id = htmlspecialchars(strip_tags($this->email));
    }


    private function setUserInfo($email)
    {
        $query = "select * from " . $this->table .
            " where email = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $email);
        $stmt->execute();
        $num = $stmt->rowCount();
        if ($num > 0) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->id = $row['id'];
            $this->full_name = $row['Full_name'];
            $this->email = $row['email'];
            $this->password = $row['password'];
            $this->userRole = $row['userRole'];
            return true;
        } else {
            return false;
        }
    }

    public function getUserInfo($email)
    {
        if ($this->setUserInfo($email)) {
            http_response_code(200);
            return json_encode(array(
                    "Id" => $this->id,
                    "email" => $this->email,
                    "password" => $this->password,
                    "full_name" => $this->full_name,
                    "userRole" => $this->userRole,

                )
            );
        } else {
            http_response_code(404);
            return json_encode(array(
                "message" => "No data found",
                "flag" => 0
            ));
        }
    }

    public function userLogIn($email, $password)
    {
        if ($this->emailExists($email)) {
            //check for password
            if (password_verify($password, $this->password)) {
                //
                http_response_code(200);
                return json_encode(array(
                    "message" => "successfully logged in",
                    "userRole" => $this->userRole,
                    "flag" => 1
                ));
            } else {
                http_response_code(401);
                return json_encode(
                    array(
                        "message" => "Unauthorized! password error",
                        "flag" => -1
                    )
                );
            }
        } else {
            http_response_code(404);
            return json_encode(
                array(
                    "message" => "User not found! check your email",
                    "flag" => 0
                )
            );
        }
    }

    private function emailExists($email)
    {

        // query to check if email exists
        $query = "SELECT password,Full_name,userRole
            FROM " . $this->table . "
            WHERE email = ?
            LIMIT 0,1";

        // prepare the query
        $stmt = $this->conn->prepare($query);
        // bind value
        $stmt->bindParam(1, $email);
        // execute the query
        $stmt->execute();
        // get number of rows
        $num = $stmt->rowCount();
        if ($num > 0) {
            //set password
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->password = $row['password'];
            $this->full_name = $row['Full_name'];
            $this->userRole = $row['userRole'];
            // return true because email exists in the database
            return true;
        }
        // return false if email does not exist in the database
        return false;
    }

    public function getUserInfoById($id)
    {
        $query = "select * from   $this->table where id = $id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $num = $stmt->rowCount();
        if ($num > 0) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            http_response_code(200);
            return json_encode(array(
                    "id" => $row['id'],
                    "email" => $row['email'],
                    "password" => $row['password'],
                    "full_name" => $row['Full_name'],
                    "userRole" => $row['userRole']
                )
            );

        } else {
            http_response_code(404);
            return json_encode(array(
                "message" => "No data found",
                "flag" => 0
            ));
        }

    }
}