<?php

class Order
{
    private PDO $conn;
    private string $table = 'orders';

    /**
     * `ID` int(11) NOT NULL,
     * `user_id` varchar(255) NOT NULL,
     * `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`data`)),
     * `washingType` varchar(255) NOT NULL,
     * `location` varchar(255) NOT NULL,
     * `paymentMethod` varchar(255) NOT NULL,
     * `cardId` varchar(255) NOT NULL
     */

    private string $id;
    private string $user_id;
    private $data;
    private string $washingType;
    private string $location;
    private $paymentMethod;
    private $cardId;
    private $status;


    public function __construct(PDO $db)
    {
        $this->conn = $db;
    }

    function registerOrder($data)
    {
        // insert query
        $query = "INSERT INTO " . $this->table . "
            SET
                user_id = ?,
                data = ?,
                washingType = ?,
                location = ?,
                paymentMethod = ?,
                cardId = ?,
                status = ?
               ";
        $stmt = $this->conn->prepare($query);
        $object = json_encode($data->cData);
        // bind the values
        $stmt->bindParam(1, $data->email);
        $stmt->bindParam(2, $object);
        $stmt->bindParam(3, $data->type);
        $stmt->bindParam(4, $data->location);
        $stmt->bindParam(5, $data->paymentMethod);
        $stmt->bindParam(6, $data->cardId);
        $stmt->bindParam(7, $data->status);

        // execute the query, also check if query was successful
        try {
            $stmt->execute();
            //201 created
            http_response_code(201);
            return json_encode(array(
                "message" => "Order registered successful",
                "flag" => 1));
        } catch (Exception $e) {
            http_response_code(400);
            return json_encode(array(
                "message" => "error: " . $e->getMessage()
            ));
        }

    }

    function updateStatus($id, $newStatus)
    {
        $query = "UPDATE  orders 
            SET status = ? where id = ?
               ";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $newStatus);
        $stmt->bindParam(2, $id);
        try {
            $stmt->execute();
            //201 created
            http_response_code(200);
            return json_encode(array(
                "message" => "Order Status changed  to " . $newStatus . " successful",
                "flag" => 1));
        } catch (Exception $e) {
            http_response_code(400);
            return json_encode(array(
                "message" => "error: " . $e->getMessage()
            ));
        }
    }

    public function getUserOrderById($id)
    {
        $query = "select * from   $this->table where user_id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $id);
        try {
            $stmt->execute();
            $num = $stmt->rowCount();
            if ($num > 0) {
                $orderArr = array();
                $orderArr['data'] = array();
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $orderItem = array(
                        "id" => $row['ID'],
                        "data" => json_decode($row['data']),
                        "washingType" => $row['washingType'],
                        "location" => $row['location'],
                        "paymentMethod" => $row['paymentMethod'],
                        "status" => $row['status'],
                        "cardId" => $row['cardId']

                    );
                    array_push($orderArr['data'], $orderItem);
                }
                http_response_code(200);
                return json_encode(
                    array(
                        "orders" => $orderArr,
                        "flag" => 1
                    )
                );
            } else {
                http_response_code(404);
                return json_encode(array(
                    "message" => "No data found",
                    "flag" => 0
                ));
            }
        } catch (Exception $e) {
            http_response_code(401);
            return json_encode(array(
                "message" => "error " . $e->getMessage(),
                "flag" => 0
            ));
        }

    }

    public function getOrders()
    {
        $query = "select * from   $this->table";
        $stmt = $this->conn->prepare($query);
        try {
            $stmt->execute();
            $num = $stmt->rowCount();
            if ($num > 0) {
                $orderArr = array();
                $orderArr['data'] = array();
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $orderItem = array(
                        "id" => $row['ID'],
                        "data" => json_decode($row['data']),
                        "washingType" => $row['washingType'],
                        "location" => $row['location'],
                        "paymentMethod" => $row['paymentMethod'],
                        "status" => $row['status'],
                        "cardId" => $row['cardId']

                    );
                    array_push($orderArr['data'], $orderItem);
                }
                http_response_code(200);
                return json_encode(
                    array(
                        "orders" => $orderArr,
                        "flag" => 1
                    )
                );
            } else {
                http_response_code(404);
                return json_encode(array(
                    "message" => "No data found",
                    "flag" => 0
                ));
            }
        } catch (Exception $e) {
            http_response_code(401);
            return json_encode(array(
                "message" => "error " . $e->getMessage(),
                "flag" => 0
            ));
        }

    }
}