<?php

require "../config/headers.php";
require "../config/Database.php";
include "../Models/Order.php";

$database = new Database();
$conn = $database->connect();
$data = json_decode(file_get_contents("php://input"));
$order = new Order($conn);
if (!empty($data->status) && !empty($data->id)) {
    echo $order->updateStatus($data->id, $data->status);
} else {
    http_response_code(403);
    return json_encode(array(
        "message" => "Error check your data!",
        "flag" => 1));
}
