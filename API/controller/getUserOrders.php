<?php
require "../config/headers.php";
require "../config/Database.php";
include "../Models/Order.php";

$database = new Database();
$conn = $database->connect();
$data = json_decode(file_get_contents("php://input"));
$order = new Order($conn);
if(!empty($data->email)){
    echo $order->getUserOrderById($data->email);
}
