<?php
require "../config/headers.php";
require "../config/Database.php";
include "../Models/Order.php";

$database = new Database();
$conn = $database->connect();
$data = json_decode(file_get_contents("php://input"));
$order = new Order($conn);
if(
    !empty($data->email)&&
    !empty($data->cData)&&
    !empty($data->paymentMethod)&&
    !empty($data->cardId)&&
    !empty($data->status)&&
    !empty($data->location)&&
    !empty($data->type)
){
    echo $order->registerOrder($data);
}
else{
    http_response_code(403);
    echo json_encode(array(
       "message" => "Check your data",
       "flag" => 0
    ));
}