<?php
require "../config/headers.php";
require "../config/Database.php";
include "../Models/User.php";

$database = new Database();
$conn = $database->connect();
$data = json_decode(file_get_contents("php://input"));
$user = new User($conn);
if (!empty($data->email) && !empty($data->password) && !empty($data->full_name) && !empty($data->userRole)) {
    echo $user->registerUser($data);
}
else{
    http_response_code(403);
    echo json_encode(array(
        "message" => "Check your data!",
        "flag" => 0
    ));
}
